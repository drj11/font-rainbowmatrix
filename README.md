# Rainbow Ripple Matrix

A series of designs based on a dot matrix font, with a rainbow
ripple colouring.

A work in progress.

The core font-design is based on the Epson FX-80 font,
from the `dotter` repo.

You will need prerequisites before you can build this font (see
next section), but if you have those then go right ahead:

Usage:

    # from this repo's top-level directory
    sh bin/rainbow.sh
    # output files are in OTF/ subdirectory.

The core idea is to ripple a repeating colour wave across the letters.
The current implementation uses a palette of 7 colours with a
selection of tiles that all tesselate by shifting a fixed number
of units to the right.
In the current implementation the thickness of the tile
(the amount by which it is shifted in a tesselation)
has to divide into 12.


## Prerequisites

- Python; frankly i have no idea which version is _required_ but
  i’m very conservative with using new things, so Python 3.6 or
  above should be fine; mostly i use whatever `brew` is happy to
  install.
- Go; 1.16 or above.
- `makeotf`, which requires [installing AFDKO](https://github.com/adobe-type-tools/afdko); it can be a
  bit of a pain

- https://gitlab.com/drj11/font-rainbowmatrix This repo in which
  you are reading the README; git clone it.
- https://git.sr.ht/~drj/dotter clone to a sibling directory of
  this one.
- https://git.sr.ht/~drj/font8 git clone; build the tools and
  install them


## Palettes

Pico-8 has a 16-colour palette with a section obviously intended
to be partly rainbow (here reduced to RGB4):

![Pico-8 palette](static/image/pico8.png)

[Kate has a pretty nice 12-colour
palette](https://iamkate.com/data/12-bit-rainbow/) and is also already in
12-bit RGB4:

![Kate-12 palette](static/image/kate12.png)

[Sasha has a 20 colour palette with 22
colours](https://sashamaps.net/docs/resources/20-colors/);
the central 10 of which are:

![Sasha-10 palette](static/image/sasha10.png)

The current implementation uses a 7 colour palette chosen from
Kate’s 12:

![KD-7 palette](static/image/kd7.png)


## The phases

The ripple works by creating a glyph entry for each pattern and
colourway of a primary glyph.
Central to this idea is _phase_.
In the ripple system _phase_ is a count of how many dot steps
there are since the beginning of a repeat.

Imagine that the tile is repeated infinitely far both left- and right-wards.
And that each new tile gets the next colour from the palette,
using the palette colours in a cycle lasting forever
(at least in our imaginations).

The 0-point of phase is arbitrary, here
i take the 0-point to be on the top-row of a tile in palette index 0, at
the leftmost filled in dot. (Note that parts of the tile may appear
on either side of the 0-point, on lower rows).

The number of dots before the pattern repeats is _P_ the period.
It is equal to _L × T_;
where:

- _L_ is the length of the palette (7, when there are 7 colours);
- _T_ is the thickness of the tile, which is
  the number of filled-in dots in each row.

In order to accomodate a primary glyph starting at any
position horitzontally, there will be _P_ colour glyphs,
one for each phase.
These colour glyphs need not have any geometry because
they will have entries in the `COLR` table which will
result in their rendering. (though the current implementation
does in fact give them geometry)

Each of these colour glyphs will need a number of layer glyphs,
one for each fragment of a tile that overlaps the primary.
For simplicity and uniformity, this implementation
has one layer glyph for each fragment of a tile that overlaps
the rectangular dot matrix that the primary could occupy;
some of these layer glyphs could be empty.

The layer glyphs are not themselves coloured, so
layers that have the same geometry can be re-used.

For any particular primary glyph we need _T_ partitions (T = tile thickness).
One for each possible position of the glyph rectangle top-left dot
within the tile.

Each partition will have a number of tile fragments, in a sequence
from left to right.

The current implementation restricts the available phases such
that the glyph rectangle top-left dot always coincideds with the
leftmost dot of the top row of the tile.
This means: _T_ must divide 12; there are fewer partitions used.


## Glyph Naming

Each primary glyph, `u0041` say, has _P_ (_L_ * _T_) colour glyphs.
They are numbered with `.p`_N_ suffix, starting from 0.
Each of these has a `COLR` entry.

Each colour glyph will have a number of layer glyphs that
partition the glyph;
which partition is used will depend on the _T_ state
(for tiles that have thickness > 1, which dot within that thickness
corresponds to the top-left of the glyph matrix rectangle).
They are number with `.t`U`c`V suffix.
Where _U_ denotes the _T_ state and _V_ gives the count of components
within that particular partition.

Glyphs for a single primary `u0041`:

- u0041
- u0041.t0c0
- u0041.t0c1
- u0041.t0c2
- u0041.t0c3
- u0041.t0c4
- u0041.t0c5
- u0041.t0c6
- u0041.t0c7
- u0041.t0c8
- u0041.t0c9
- u0041.t0c10
- u0041.p0
- u0041.p1
- u0041.p2
- u0041.p3
- u0041.p4
- u0041.p5
- u0041.p6

The number of _c_ glyphs is determined by the overall shape of the tile
(and the width of the glyph, but in this version they are all
monospaced).

The number of _p_ glyphs is the number of distinct phases that
are used.


## Implementation Ideas

Each glyph consists of W vertical stripes (W=12)

Idea A is to split each dotter glyph into its W vertical stripes, and
render it with each stripe being a different colour.
5 or 7 stripes will ensure that the pattern has the longest
cycle before repeating (gcd(5,12)==gcd(7,12)==1)

Later, after trying this (and it seems a bit ugly):
change palette after every S stripes.
There are K palette entries.
In general there will be S*K possible starting states (phases)
for a glyph.
If S divides W then there are K possible phases.
If gcd(K, W) is > 1 then there will be fewer than K possible
phases (W/gcd(K,W) I think).

glyph.colour (below) is done, and meant that i now realise that
a single COLR entry represents a multi-layer multi-colour glyph.
Each grey glyph needs K COLR glyphs (one for each starting
colour), each of which will have W (or 11, because the
right-hand column is always empty) layers.

Why not have K layers (one for each colour)? Because then if
layers overlap (which they will when radius>0.5), they will not
overlap in a strictly left-to-right fashion. So we have 12
layers, some of which will re-use a palette entry.

That suggests a slightly different approach:

- dotter.decomp: decompose the glyphs of dotter into
  W single-slice glyphs, together forming a _partition_
  (some of these may be empty, depending on how we allocate
  slices).
- dotter.phase: effectively each grey glyph has L colour
  counterparts, each starting with a different phase.
  Each different colourway for a glyph requires a new
  placeholder glyph to be synthesized.
  Construct the COLR entries for each colourway of each glyph.
  (len(COLR) = L × len(glyf))
- phase.feature: OTL feature rules that select an initial phase
  colour, and for each of L phases select the next phase.
  For the initial rule we can either simply or pseudo-randomly
  pick a phase depending on the first glyph.
  for the subsequence rule a left-context will reliably predict
  the next phase, and L groups can be used with L rules.

old approach to rainbow ripple:

- dotter.decomp: decompose the glyphs of dotter into 12 slices
  each (the right-hand slice will always be empty).
  The dot matrix suggest a decomp into 12 slices (for
  efficiency), but in general a decomp into K slices, where K is
  the number of colours is sufficient (i think).
- feature.slice: OTL feature rules that decompose incoming
  full-width glyphs into slices.
- slice.expand: expanding the slices, creating K glyphs for each slice,
  one for each colour.
- feature.ripple: OTL feature rules that select the "next"
  colour in the ripple sequence. Approximately:
    sub [@colourN] [@grey]' by [@colourSN];
- glyph.colour: literally make any glyph some colour (tech
  improvement)
- slice.colour: assign colours to all the K-expanded slices, as
  appropriate

# END
