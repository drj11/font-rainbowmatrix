#!/usr/bin/env python

# https://docs.python.org/3.5/library/argparse.html
import argparse

# https://docs.python.org/3.5/library/collections.html
import collections

# https://docs.python.org/3.6/library/importlib.html
import importlib

# https://docs.python.org/3.5/library/math.html
import math

# https://docs.python.org/3.5/library/os.html
import os

# https://docs.python.org/3.5/library/pathlib.html
import pathlib

import sys


class Error(Exception):
    ...


# The numeric based names for tile shapes use a S / A / T system:
# S = Shape (1 = stripe, 3 = stipple, 4 = chevron)
# A = Angle (0 is vertical, 1 slight NE slope, 9 slight NW slope)
# T = Thickness
# (After Helvetica and Bitstream)
TileDict = {
    # vert1=("#\n" * 9).strip(),
    "102": ("##\n" * 9).strip(),
    "104": ("####\n" * 9).strip(),
    "106": ("######\n" * 9).strip(),
    "10C": ("############\n" * 9).strip(),
    # deckle1="#\n-#\n#\n-#\n#\n-#\n#\n-#\n#",
    "deckle2": "##-\n-##\n##-\n-##\n##-\n-##\n##-\n-##\n##-",
    "302": "##--\n--##\n##--\n--##\n##--\n--##\n##--\n--##\n##--",
    "412": "##\n-##\n--##\n---##\n----##\n---##\n--##\n-##\n##",
    "492": "----##\n---##\n--##\n-##\n##\n-##\n--##\n---##\n----##",
    "112": "--------##\n-------##\n------##\n-----##\n----##\n---##\n--##\n-##\n##",
    "192": "##\n-##\n--##\n---##\n----##\n-----##\n------##\n-------##\n--------##",
    "194": "####\n-####\n--####\n---####\n----####\n-----####\n------####\n-------####\n--------####",
    "196": "######\n-######\n--######\n---######\n----######\n-----######\n------######\n-------######\n--------######",
    "19C": "############\n-############\n--############\n---############\n----############\n-----############\n------############\n-------############\n--------############",
}


# We're assuming the RGB4 CPAL palette.
# In which the 12-bit indices are hex RGB4 colour values
# (because of the ordering within the palette).
PaletteDict = dict(
    cga7lo=[0x000, 0x00A, 0x0A0, 0x0AA, 0xA00, 0xA0A, 0xA50],
    cga1hi=[0x000, 0x5FF, 0xF5F],
    cga0hi=[0x000, 0x5F5, 0xF55, 0xFF5],
    mrb=[0xF5F, 0xF55, 0x000, 0x55F],
    g4=[0x333, 0x666, 0x999, 0xCCC],
    kt7=[
        0xA35,
        0xE94,
        0xED0,
        0x9D5,
        0x2BC,
        0x36B,
        0x639,
    ],
    p2=[0xA5A, 0xA50],
    tslover=[0x856, 0xB36, 0xEBC],
    tsredtv=[0x400, 0x720, 0x976, 0xB86],
)


def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--tile", help="shape of tile (see --list-tile)")
    group.add_argument(
        "--list-palette", action="store_true", help="show available palettes"
    )
    group.add_argument("--list-tile", action="store_true", help="show available tiles")
    parser.add_argument("--dot", default="circle", help="shape the dot")
    parser.add_argument("--palette", default="kt7", help="choose palette")
    parser.add_argument("--radius", type=float, help="radius of dot")
    parser.add_argument(
        "--hpitch", metavar="DECIPOINT", type=float, help="horizontal pitch"
    )

    arg = parser.parse_args()

    if arg.list_palette:
        for n in sorted(PaletteDict.keys()):
            print(n)
        return 0

    if arg.list_tile:
        for n in sorted(TileDict.keys()):
            print(n)
        return 0

    # All these imports are from dotter.
    # They are imported here so that --list-palette and
    # --list-tile still work even when PYTHONPATH isn't set correctly.
    globals()["dot"] = importlib.import_module("dot")
    globals()["agl"] = importlib.import_module("agl")
    globals()["series"] = importlib.import_module("epsonfx80cubi")

    base = "rainbow"

    dir = pathlib.Path(base)
    os.makedirs(dir, exist_ok=True)

    makesvg(dir, base, arg)


def TileFromName(name):
    """Return a tile shape given a name.
    In this implementation this is a simple dict lookup,
    but later it may involve generation / computation.
    """

    tile = TileDict[name]
    valid_tile(tile)
    return tile


def makesvg(dir, base, arg):
    docem = 12
    upem = 1440

    tile = TileFromName(arg.tile)
    codename = arg.tile
    psname = "RainbowRippleMatrixMono" + codename
    assert "\n" not in psname

    with (open(dir / "FontMenuNameDB", "w") as fontmenuname,):
        fontmenuname.write("[%s]\n" % psname)
        fontmenuname.write("f=Rainbow Ripple Matrix Mono %s\n" % codename)

    with (
        open(dir / ("control-" + base + ".csv"), "w") as control,
        open(dir / (base + ".svg"), "w") as wsvg,
        open(dir / ("feature-" + base + ".fea"), "w") as feature,
    ):
        control.write("name,Win/BMP,en-US,6,%s\n" % psname)
        control.write("docem,{}\n".format(docem))
        control.write("upem,{}\n".format(upem))
        ascender = round(upem * 7 / 12)
        descender = round(upem * 2 / 12)
        linegap = upem - (ascender + descender)
        control.write("os/2,TypoAscender,{:d}\n".format(ascender))
        control.write("os/2,TypoDescender,{:d}\n".format(-descender))
        control.write("os/2,TypoLineGap,{:d}\n".format(linegap))

        feature.write(
            """languagesystem DFLT dflt;
"""
        )

        # https://github.com/adobe-type-tools/afdko/blob/develop/docs/OpenTypeFeatureFileSpecification.md
        feature.write(
            """table OS/2 {
    TypoAscender %d;
    TypoDescender %d;
    TypoLineGap %d;
} OS/2;

"""
            % (ascender, -descender, linegap)
        )

        feature.write(
            """
feature calt {
"""
        )

        print(
            """<?xml version="1.0" encoding="UTF-8"?>
<svg
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  width="4in" height="6in"
  viewBox="0 0 288 432">
""",
            file=wsvg,
        )

        radius = arg.radius or 0.52
        hpitch = 5
        vpitch = 1.0

        palette = [0xBCD, 0xCCE, 0xC78, 0x476, 0xDA4]
        palette = PaletteDict[arg.palette]

        # feature list containing primary glyphs (mapped
        # directly from unicode, not placeholders for colour or layers).
        primary_list = []

        wsvg.write(dot.SVGSymbol(dot.DotSelect[arg.dot], radius))

        tile = tile.split()
        T = tile[0].count("#")

        L = len(palette)
        # number of different phases
        P = L * T
        init_phase_list = [[] for _ in range(P)]
        next_phase_list = [[] for _ in range(P)]

        for i, glyph in enumerate(series.font):

            matrix = ToMatrix(glyph.columns)
            # Width of glyph (in slices)
            W = 12
            if W % T != 0:
                raise Error(
                    "For this implementation, T, %d, must divide W, %d" % (T, W)
                )

            primary_list.append(glyph.name)

            WriteGlyph(wsvg, glyph.name, matrix, hpitch, vpitch)

            # Make a sequence of layer glyphs for each partition.
            # For now, only one partition.
            partition = partition_t(matrix, W, tile, 0)
            for c, fragment in enumerate(partition):
                id_layer = "{name}.t{t}c{c}".format(name=glyph.name, t=0, c=c)
                WriteGlyph(wsvg, id_layer, fragment.m, hpitch, vpitch)

            # For each possible phase, make:
            # - a glyph, in the SVG file
            # - a COLR entry, in the control file.
            # - an entry in the begin_phase and next_phase lists
            for phase in range(0, P, T):
                id_phase = f"{glyph.name}.p{phase}"
                WriteGlyph(wsvg, id_phase, matrix, hpitch, vpitch)
                init_phase_list[phase].append(id_phase)
                for i, fragment in enumerate(partition):
                    palette_index = (phase // T + fragment.pal) % L
                    id_layer = f"{glyph.name}.t0c{i}"
                    control.write(
                        "colr,0,base,{id_phase},{i},{id_layer},0x{pal:03x}\n".format(
                            id_phase=id_phase,
                            i=i,
                            id_layer=id_layer,
                            pal=palette[palette_index],
                        )
                    )

                # each next phase list records the list of
                # glyphs that will be followed by a particular
                # phase.
                next_phase = (phase + W) % P
                next_phase_list[next_phase].append(id_phase)

        # In the feature file, create named groups...
        # ... one for each initial phase
        for phase, l in enumerate(init_phase_list):
            if len(l) == 0:
                continue
            feature.write(
                "  @init.p{phase} = [{j}];\n".format(phase=phase, j=" ".join(l))
            )
        # ... one for each next phase
        for phase, l in enumerate(next_phase_list):
            if len(l) == 0:
                continue
            feature.write(
                "  @next.p{phase} = [{j}];\n".format(phase=phase, j=" ".join(l))
            )
        # ... and one for the primaries.
        feature.write("  @primary = [{j}];\n".format(j=" ".join(primary_list)))

        # Ripple rules
        for p in range(P):
            if len(next_phase_list[p]) == 0:
                feature.write("#")
            feature.write("  sub @next.p{p} @primary' by @init.p{p};\n".format(p=p))

        for id in primary_list:
            # a rule that kickstarts the ripple.
            # the prime, «'», is apparently required. It forces
            # this rule to be a Contextual, putting it in the same
            # lookup as the previous.
            # I don't understand why this is needed though.
            feature.write("  sub {primary}' by {primary}.p0;\n".format(primary=id))

        feature.write("""} calt;\n""")

        wsvg.write("""</svg>""")


# m is the matrix, a sequence of integers, for the fragment,
# pal is the palette offset. The fragment that would contains
# the top-left point has offset 0, ones to the right have
# successive palette offets, ones to the left have
# successively negative offsets.
Fragment = collections.namedtuple("Fragment", "m pal")


def partition_t(matrix, W, tile, t):
    """Partition (split) the matrix `matrix` of assumed width `W`
    into a series of fragments each of which is the tile shifted
    (by a multiple of T) and intersected with `matrix`.
    Together, the fragments sum (union) to matrix.
    The unique pal 0 fragment can have T possible positions
    (each one corresponds to the top-left dot of the matrix
    rectangle being a different dot across the thickness of the tile).
    `t` gives that position.
    """

    assert 0 == t

    # extend matrix to width W
    matrix = matrix + [0] * (W - len(matrix))

    T = tile[0].count("#")

    """
    --###--
    ###----
    --###--
    ---###-
    ----###
    ---###-
    """

    E = max(len(row) for row in tile)
    # number of dots to the right of the top row of tile
    right_blank = E - tile[0].rindex("#") - 1
    # number of dots to the left of the top row of tile
    left_blank = tile[0].index("#")
    # right_pad should be largest integer < W such that
    # right_pad + right_blank is a multiple of T.
    right_excess = (W - 1 + right_blank) % T
    right_pad = W - 1 - right_excess
    assert (right_pad + right_blank) % T == 0
    assert right_pad < W
    assert W <= right_pad + T
    # `pal` is the palette offset for each fragment.
    # The fragment that contains the top-left dot of the
    # matrix rectangle has offset 0 (+ve to right, -ve to left).
    # `pal` is initialised here to the pal of the first fragment
    # we generate (the most -ve one).
    # The bracketed expression below is the number of dots
    # between the left-most dot of the tile top-row and the
    # left-most dot of the W wide window of the first fragment.
    pal = -(right_pad + right_blank + T - W) // T
    assert pal <= 0

    # left_pad should be the largest integer < W such that
    # left_pad + left_blank is a multiple of T.
    left_excess = (W - 1 + left_blank) % T
    left_pad = W - 1 - left_excess
    assert (left_pad + left_blank) % T == 0
    assert left_pad < W
    assert W <= left_pad + T
    # :todo: adjust above for when t != 0

    # Convert from string to list model
    tile = TileToMatrix(tile)

    # pad on left and right, then take successive "windows".
    tile = [0] * left_pad + tile + [0] * right_pad
    # With padding, the tile should be a multiple of T columns.
    assert len(tile) % T == 0

    es = []
    # Take a window, starting on the right so that the tile
    # appears in its left-most position to begin with,
    # and mask it against the glyph in matrix;
    # shifting by T to produce successive "stamps".
    for i in reversed(range(0, len(tile) + 1 - W, T)):
        m = tile[i : i + W]
        for c in range(W):
            m[c] &= matrix[c]
        es.append(Fragment(m, pal))
        pal += 1
    assert 0 <= pal

    return es


def matrixroll(m, W, shift):
    """Roll the matrix, which should be of width W, by `shift`
    positions.
    """

    assert len(m) == W
    assert 0 < shift < W
    return m[-shift:] + m[:-shift]


def valid_tile(tile):
    """The `tile` is valid if:
    - it is split by \n into 9 elements
    - each element a string
    - each string contains the same number of "#"
    - it is presented in leftmost position, so
      at least one element starts with "#"
    """

    tile = tile.split("\n")
    assert 9 == len(tile)
    n = tile[0].count("#")
    for s in tile:
        assert isinstance(s, str)
        assert s.count("#") == n
    assert any(e.startswith("#") for e in tile)


def WriteGlyph(w, id, matrix, hpitch, vpitch):
    """To the output file w, write a SVG <g> element
    for the glyph made from the dots in matrix.
    """
    dots = dotsfor(matrix, hpitch * 0.5 / 10, 0.5, hpitch, vpitch)

    w.write("""<g id="{id}">""".format(id=id))
    w.write(
        """<path class="baseline" d="M {} 7 h {} Z" />
""".format(
            -hpitch * 0.75 / 10, hpitch * 12 / 10
        )
    )
    for x, y in dots:
        w.write("""<use x="{x}" y="{y}" href="#dot" />\n""".format(x=x, y=y))
    w.write("""</g>\n""")


def TileToMatrix(pattern):
    """Convert to the 9-pin matrix format, which is
    a list of 9-bit integers.
    """

    w = max(len(row) for row in pattern)
    rows = [(row + "-" * w)[:w] for row in pattern]
    columns = []
    for i in range(w):
        columns.append([row[i] for row in rows][::-1])

    m = ToMatrix(columns)
    # trim excess columns
    while m[-1] == 0:
        del m[-1]
    return m


def ToMatrix(columns):
    """Convert columns, a list of sequence of elements,
    to matrix, a list of (9-bit) integers.
    """

    m = []
    for column in columns:
        colint = sum([(e == "#") << i for i, e in enumerate(column)])
        m.append(colint)
    return m


def dotsfor(matrix, left, top, hpitch, vpitch):
    """
    Return the dots for the "on" pins of the matrix
    the matrix is in column major format:
    each element is a column of 9 pins, each
    column represents as a 9 bit integer.
    Most significant bit (256) is topmost.
    hpitch is in tenths; vpitch is not.

    left and top give the position of the centre of
    the dot at top-left (or would be if that position was used)
    """

    # y _increases_ downwards;
    # bit _decreases_ downwards.

    dots = []
    for col, p in enumerate(matrix):
        for row, bit in enumerate(range(9)[::-1]):
            if p & (1 << bit):
                dots.append((left + (col * hpitch) / 10, top + row * vpitch))
    return dots


if __name__ == "__main__":
    sys.exit(main())
