#!/bin/sh

# Primary control script for making the Rainbow Ripple series.
# The shapes are slices of the Epson FX-80 font.
# The underlying FX-80 shapes are the responsibility of `dotter`;
# the rainbowsource.py Python script slices those up for this
# font series.
# The Font 8 `ttf8` tools make an intermediate font, which
# then has smart rules added by `makeotf`.
# The smart rules are in the form of AFDKO feature syntax.

set -e

cd ${PWD%font-rainbowmatrix*}font-rainbowmatrix

tiles=$(
  PYTHONPATH=$PWD/../dotter python sources/rainbowsource.py --list-tile
)

mkdir -p fonts/OTF
rm -f fonts/OTF/* OTF/*

# Make a single Rainbow Ripple OTF file in the OTF directory.
rainbow1 () {
    rm -f rainbow/*

    PYTHONPATH=$PWD/../dotter python sources/rainbowsource.py "$@"

    ttf8 --dir rainbow --control rainbow/control-rainbow.csv rainbow/rainbow.svg
    f8name --dir rainbow --control rainbow/control-rainbow.csv
    cp RGB4.CPAL rainbow/CPAL
    colr --dir rainbow --control rainbow/control-rainbow.csv
    assfont -dir rainbow

    G=rainbow/GlyphOrderAndAliasDB

    glys rainbow.ttf | while read a
      do
      printf "%s\t%s\n" "$a" "$a"
      done > $G

    OTFName=fonts/OTF/ripple-"$2".otf

    makeotf -ga -ff rainbow/feature-rainbow.fea -f rainbow.ttf -gf "$G" -mf rainbow/FontMenuNameDB -o "$OTFName"

    # plak --order code "$OTFName" | kitty icat
}

if [ $# != 0 ]
then
    # Assume single shot case, pass everything to rainbow1
    rainbow1 "$@"
else
    # do all variants
    for tile in $tiles
    do
        rainbow1 --tile "$tile"
    done
fi

for OTFName in $(find fonts -name '*.otf')
do
    hb-view "$OTFName" ' Dolphins
NNNNNNNNN
█████████' | kitty icat
done
